

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
const minify = require('gulp-js-minify');
const uglify = require('gulp-uglify');
const cleanCss = require('gulp-clean-css');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');

// Пути к файлам и папкам
const paths = {
  src: {
    root: 'src',
    html: 'src/index.html',
    scss: 'src/style/*.scss',
    js: 'src/js/**/*.js',
    img: 'src/img/**/*',
  },
  dist: {
    root: 'dist',
    css: 'dist/css',
    js: 'dist/js',
    img: 'dist/img',
  },
};

// Задача для компиляции SCSS в CSS
function compileSass() {
  return gulp
    .src(paths.src.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(cleanCss())
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest(paths.dist.css));
}

// Задача для конкатенации и минификации JS файлов
function minifyJS() {
  return gulp
    .src(paths.src.js)
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.dist.js));
}

// Задача для оптимизации изображений
function optimizeImages() {
  return gulp
    .src(paths.src.img)
    .pipe(imagemin())
    .pipe(gulp.dest(paths.dist.img));
}

// Задача для очистки папки dist
function cleanDist() {
  return gulp.src(paths.dist.root, { read: false, allowEmpty: true }).pipe(clean());
}

// Задача для копирования index.html в папку dist
function copyHtml() {
  return gulp.src(paths.src.html).pipe(gulp.dest(paths.dist.root));
}

// Задача для отслеживания изменений и автоматической пересборки
function watchFiles() {
  gulp.watch(paths.src.scss, compileSass);
  gulp.watch(paths.src.js, minifyJS);
  gulp.watch(paths.src.html).on('change', browserSync.reload);
}

// Задача для запуска сервера BrowserSync
function serve() {
  browserSync.init({
    server: {
      baseDir: paths.dist.root,
    },
  });

  gulp.watch(paths.src.scss, compileSass);
  gulp.watch(paths.src.js, minifyJS);
  gulp.watch(paths.src.img, optimizeImages);
  gulp.watch(paths.src.html).on('change', gulp.series(copyHtml, browserSync.reload));
}

// Задача build
const build = gulp.series(
  cleanDist,
  gulp.parallel(compileSass, minifyJS, optimizeImages),
  copyHtml
);

// Задача dev
const dev = gulp.series(build, gulp.parallel(serve, watchFiles));

exports.build = build;
exports.dev = dev;
exports.default = dev;
